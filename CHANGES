1.2.3 2023-02-26
 - Improve support for non-ASCII email messages.  Anything UTF-8 should work
   (including correct signing/verification).  For messages that contain header
   fields with non-ASCII or UTF-8 content, signatures are likely fail
   verification, but the milter should continue to run.  (Thanks to Casper
   Bruun for help with this)
 - Set minimum pymilter and dkimpy versions in setup.py to those that will
   work reliably with non-ASCII content.
 - Fixed support for percent in KeyTable - Thanks to Mika Tiainen
 - Fix formatting for MinimumKeyBits in dkimpy-milter.conf(5)
   (Closes: #995335)
 - Reset the i= signature identity in get_identities_sign() (Closes: #981157)
 - Improve documentation of inter-relationship between Mode, InternalHosts,
   MacroList, and MacroListVerify options in dkimpy-milter.conf.5 (Closes:
   #969215)
 - Fix subdomain signing with top-level organizational domain (LP: #1999434)
   - Thanks to Matthias Hunstock for the report and the fix
 - Fix comma separated list processing in dkimpy_milter/config.py
   (LP: #1901445)

1.2.2 2020-08-09
 - Improve README.md formating for markdown display on pypi
 - Improve documentation in dkimpy-milter.conf (5) and README.md for signing
   for multiple domains (Thanks to Stefano Rivera)
 - Minimal fix for dnspython 2.0.0 compatibility (still works with 1.16.0)

1.2.1 2020-01-04
 - Fix expand option not to fail if files are missing since socket activation
   service files are not shipped in the sdist
 - Correct dkimpy-milter.conf file install location to match expand locations

1.2.0 2020-01-03
 - Add support for SigningTable, KeyTable, and KeyTableEd25519 (LP: #1797397)
 - Add support for specifying MinimumKeyBits for RSA signatures
 - Add support for SignHeaders feature, thanks to Ralph Seichter for the patch
 - Add support for specifying DNSTimeout (bumps required dkimpy version to 1.0)
 - Add information on message content conversion to README
 - Add new expand option to setup.py so various file system locations can be
   specified at build/install time rather than being hard coded
 - Install openrc init file for Gentoo and other openrc users
 - Add support for passing PID file name on command line to make it easier to
   keep system init and daemon configuration in sync
 - Add support for storing DKIM failed mails in a specified
   DiagnosticDirectory
 - Fix startup logging so it provides information at a useful time
 - Fix verify processing so missing (optional) i= tag doesn't cause the milter
   to fail  (LP: #1842250)
 - Fix message extraction so that signing in the same pass through the milter
   as verifying works correctly
 - Add debug logging for content type to assist troubleshooting MIME
   conversion issues
 - Fix variable initialization so mailformed mails missing body From do not
   cause a traceback (LP: #1844161)
 - Catch more ascii encoding errors to improve resilience against bad data
   (LP: #1844189)
 - Fix sysv init so it works (LP: #1839487)
 - Make error logging more explicit to aid debugging
 - Remove SigningTableEd25519 from documentation - it was never implemented
   and a per algorithm signing table turns out not to be needed
 - Delete own_socketfile to resolve race condition where the permissions
   change fails on a Unix socket because it hasn't been created yet (libmilter
   will do this correctly on its own based on umask, the milter doesn't need
   to do it) (LP: #1849712)

1.1.0 2019-04-12
 - Add SubDomains option to enable signing for sub-domains (LP: #1811535)
 - Port to python3 (LP: #1815502)
 - Add test suite using opendkim miltertest
 - When Socket is absolute path, do not strip leading /
 - Handle unix: socket prefix the same as local:
 - Set up correct AuthservID defaults
 - config: Reassemble strings sensibly
 - Consistently prefer dnspython to Py3DNS (LP: #1815558)

1.0.1 2019-02-11
 - Reorder milter start and dropping privileges so permissions on Unix socket
   are correct (LP: 1797720)
 - Make domain checks case insensitive for determining if signing should be
   done (LP: #1815311)
 - Add additional Sendmail configuration information to README from OpenDKIM
   update based on input from Дилян Палаузов (LP: #1801619)
 - Add information on Ed25519 key creation to README (LP: #1815313)

1.0.0 2018-05-11
 - Minor documentation updates
 - Deleted reference to obsolete syslog target in unit file

0.9.7 2018-03-19
 - Made sysv init executable
 - Add missing documentation key to system/dkimpy-milter.service
 - Put version directly in setup.py and do not import dkimpy_milter to ease
   install via pip
 - Minor sysv init improvments

0.9.6 2018-03-13
 - Fixed typo in package installation section of README
 - Added more to README about first run with systemd
 - Fixed typo in path for fallback location of the config file if one is not
   provided
 - Added protection for malformed From addresses.  If the From does not at
   least have an '@' in the address, then the signing domain is not extracted
   and the message will not be signed

0.9.5.1 2018-03-10
 - Add conf file location to systemd unit file
 - Fix setup.py install locations so they are installed correctly

0.9.5 2018-03-10
 - Beta 1 (updated Alpha -> Beta warning in README and trove classifiers)
 - Added support for MacroList option
 - Added support for MacroListVerify option
 - Added example in README to show use of MacroList* to separate inbound and
   outbound mail streams
 - Added support for SyslogSuccess option (both signing and verifying)
 - Rationalized logging to be much less verbose unless SyslogSuccess or
   debugLevel are set - default is generally start/stop/errors only
 - Fixed install_requires so either dnspython (preferred if neither is
   installed) or PyDNS satisfies the install requirements
 - Updated Authentication Results result comment not to mention key size for
   ed25519 signatures, since it's irrelevant
 - Enhanced signature verification logging to provide more useful information

0.9.4 2018-03-09
 - Create PID directory if it is missing
 - Fix crash when verifying if domain for signing was not set
 - Fix header folding to use \n only to align with milter protocol
   requirements
 - Added information about creating a dedicated user and PID file directory
   creation to README
 - Fixed a bug where dkim fail might be reported as pass when verifying
   multiple signatures and a previous signature had passed
 - Make RSA signatures in dkimpy-milter optional, so dkimpy-milter can be
   added after an existing DKIM signing application to add an Ed25519
   signature (Thanks to A. Schulze for the patch)
 - Added support for AuthservID option
 - Added support for InternalHosts option (ipaddress and either dns (dnspython)
   or pydns (DNS) modules are now required)
 - Added support for DiagnosticDirectory and updated dkimpy-milter specifics in
   dkimpy-milter.conf.5

0.9.3 2018-03-02
 - Fixup csl dataset processing for single item lists
 - file: dataset support
 - Bump minimum authres version to 1.1.0 due to known issues with 1.0.2
 - Ignore errors parsing broken authres header fields
 - Fold added authres header fields
 - Fix pidfile permissions
 - Fix socket setup sequence so Unix sockets work

0.9.2 2018-02-19
 - Improved package requirements definition
 - Added systemd unit file and (untested) sysv init file
 - Added dkim-milter.8 (based on opendim.8)
 - Implemented support for Canonicalization option
 - Implemented support for SyslogFacility option
 - Initial dataset support: csl
 - Only sign if mail from from a domain in Domain and only if Mode is not
   verfication only
   
0.9.1 2018-02-17
 - DKIM signing and verification using both RSA and Ed25519
 - The following configuration options are supported (same definition as
   OpenDKIM): Domain, KeyFile, KeyFileEd25519, Mode, PidFile, Selector,
   Socket, Syslog, UMask, and UserID (see dkimpy-milter.conf.5)
 - This is an Alpha grade release and while the implemented features work, it
   is nowhere near being a complete package

